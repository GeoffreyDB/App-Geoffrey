
use strict;
use warnings;
use File::Spec;
use Test::More;

eval "use Test::Cmd";
if ($@) {
    plan skip_all => "Test::Cmd required";
}
else {
    plan tests => 6;
}

my $cmd = Test::Cmd->new(
    workdir => '',
    prog    => File::Spec->catfile( 'bin', 'geoffrey' ),
);

ok( $cmd->run( args => '-h' ),     'geoffrey help ok' );
ok( $cmd->run( args => '--help' ), 'geoffrey help ok' );
ok( $cmd->run( args => '-?' ),     'geoffrey help ok' );
ok( !$cmd->run( args => '--version' ), 'geoffrey current version' );
ok( !$cmd->run( args => '-v' ),        'geoffrey current version' );

ok( !$cmd->run( args => 'changeset -d test' ), 'geoffrey create changeset' );

#my $chglgs = File::Spec->catfile( $FindBin::Bin, 'data', 'changelog' );
#my $db_file = File::Spec->catfile( $FindBin::Bin, '.tmp.sqlite' );
#ok( !$cmd->run( args => 'read -b=' . $db_file . ' -r -d=' . $chglgs ), 'geoffrey run ok' );

#unlink $db_file or warn "Could not unlink $db_file: $!";
