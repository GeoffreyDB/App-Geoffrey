use Test::More tests => 4;

use FindBin;
use lib File::Spec->catfile( $FindBin::Bin, '..', 'lib' );
use strict;
use warnings;

require_ok('Geoffrey');
use_ok 'Geoffrey';

require_ok('Getopt::Long');
use_ok 'Getopt::Long';

