
use strict;
use warnings;
use FindBin;
use File::Spec;
use File::Path qw(remove_tree);
use Test::More tests => 8;

require_ok('FindBin');
use_ok 'FindBin';

require_ok('File::Spec');
use_ok 'File::Spec';

require_ok('File::Path');
use_ok 'File::Path';

require_ok('App::Geoffrey::Converter');
use_ok 'App::Geoffrey::Converter';

my $path = File::Spec->catfile( $FindBin::Bin, '.tmp' );

my $config = {
    author => 'Test Author',
    email  => 'author@cpan.org',
    driver => 'MySQL',
    dir    => $path
};

App::Geoffrey::Converter->new()->make($config);

# TODO sub test for created file
remove_tree $path or die "Could not unlink $path: $!";
