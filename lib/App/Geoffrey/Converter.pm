package App::Geoffrey::Converter;

use strict;
use warnings FATAL => 'all';
use Geoffrey::Utils;
use File::Path qw( mkpath );

use parent 'App::Geoffrey::Base';

$App::Geoffrey::Converter::VERSION = '0.000100';

sub file {
    return q~package Geoffrey::Converter::{0};

$Geoffrey::Converter::{0}::VERSION = '0.1.0';

use {4};

sub _min_version { '0' }

=head1 SUBROUTINES/METHODS

=head2 generate_unique

=cut

sub generate_unique {}

=head2 generate_foreign_key

=cut

sub generate_foreign_key {}

=head2 add_column

=cut

sub add_column {}

1;  # End of Geoffrey::Changelog::{0}

__END__

=head1 NAME

Geoffrey::Converter::{0} - A new Geoffrey::Converter module!

=head1 VERSION

Version 0.1.0

=head1 BUGS

Please report any bugs or feature requests to C<bug-Geoffrey-driver-{0} at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Geoffrey-Converter-{0}>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Geoffrey::Converter::{0}


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Geoffrey-Converter-{0}>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Geoffrey-Converter-{0}>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Geoffrey-Converter-{0}>

=item * Search CPAN

L<http://search.cpan.org/dist/Geoffrey-Converter-{0}/>

=back

=head1 ACKNOWLEDGEMENTS

=head1 AUTHOR

{1}, C<< <{2}> >>

~;
}

sub make {
    my ( $self, $config ) = @_;
    die "No author defined!"       unless $config->{author};
    die "No mail address defined!" unless $config->{email};
    die "No new driver defined!"   unless $config->{driver};
    my $dir = "$config->{dir}/Geoffrey-Converter-$config->{driver}";
    mkpath( "$dir/lib/DBIx/Schema/Changelog/Converter", 0755 );
    mkpath( "$dir/t",                                   0755 );

    require Geoffrey::Utils;

    # #module
    # my $path = File::Spec->catfile( $dir, 'lib', 'DBIx', 'Schema', 'Changelog', 'Converter',
    #     "$config->{driver}.pm" );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->file(),
    #         [
    #             $config->{driver}, $config->{author}, $config->{email}, $self->year(),
    #             '5.10.0'
    #         ]
    #     )
    # );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->license(), [ $self->year(), $config->{author} ]
    #     )
    # );
    # _write_file( $path, qq~\n=cut\n~ );

    # #AUXILIARY
    # $path = File::Spec->catfile( $dir, 'README.md' );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->readme(),
    #         [ 'Converter', $config->{driver}, $config->{author}, $config->{email} ]
    #     )
    # );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->license(), [ $self->year(), $config->{author} ]
    #     )
    # );
    # $path = File::Spec->catfile( $dir, 'Makefile.PL' );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->makefile(),
    #         [
    #             'Converter',          $config->{driver},
    #             $config->{author}, $config->{email},
    #             $App::Geoffrey::Converter::VERSION
    #         ]
    #     )
    # );
    # $path = File::Spec->catfile( $dir, 'Changes' );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->changes(),
    #         [ 'Converter', $config->{driver}, '0.0.0_001', '1970/01/01', $config->{author} ]
    #     )
    # );
    # $path = File::Spec->catfile( $dir, 'MANIFEST' );
    # _write_file( $path,
    #     Geoffrey::Utils::replace_spare( $self->manifest(), [ 'Converter', $config->{driver} ] ) );

    # #tests
    # $path = File::Spec->catfile( $dir, 't', '00-load.t' );
    # _write_file( $path,
    #     Geoffrey::Utils::replace_spare( $self->t_load(), [ 'Converter', $config->{driver} ] ) );
    # $path = File::Spec->catfile( $dir, 't', 'boilerplate.t' );
    # _write_file(
    #     $path,
    #     Geoffrey::Utils::replace_spare(
    #         $self->t_boilerplate(),
    #         [ 'Converter', $config->{driver} ]
    #     )
    # );
    # $path = File::Spec->catfile( $dir, 't', 'manifest.t' );
    # _write_file( $path, Geoffrey::Utils::replace_spare( $self->t_manifest(), [] ) );
    # $path = File::Spec->catfile( $dir, 't', 'pod-coverage.t' );
    # _write_file( $path, Geoffrey::Utils::replace_spare( $self->t_pod_coverage(), [] ) );
    # $path = File::Spec->catfile( $dir, 't', 'pod.t' );
    # _write_file( $path, Geoffrey::Utils::replace_spare( $self->t_pod(), [] ) );
}

1;

__END__

=head1 NAME

App::Geoffrey::Converter - Create a new driver module from template for Geoffrey!

=head1 VERSION

Version 0.000100

=head1 SUBROUTINES/METHODS

=head2 file

=head2 make

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, trade name, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANT ABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
