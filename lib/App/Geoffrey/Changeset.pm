package App::Geoffrey::Changeset;

use utf8;
use 5.010;
use strict;
use warnings;
use Geoffrey;

use parent 'App::Geoffrey::Base';

$App::Geoffrey::Changeset::VERSION = '0.000100';

sub db     { return $_[0]->{db}; }
sub user   { return $_[0]->{user}; }
sub host   { return $_[0]->{host}; }
sub pass   { return $_[0]->{pass}; }
sub driver { return $_[0]->{driver}; }
sub type   { return $_[0]->{type}; }

sub make {
    my ($self) = @_;

    require File::Path;
    File::Path::mkpath( File::Spec->catfile( $self->dir(), 'changelog' ), 0755 );
    require Geoffrey::Utils;

    #_write_file(
    #File::Spec->catfile( $self->dir(), 'changelog', 'changelog' )
    #  . $self->loader()->ending(),
    #    Geoffrey::Utils::replace_spare( $self->loader()->tpl_main(), [] )
    #File::Spec->catfile( $self->dir(), 'changelog', 'changelog-01' )
    #    . $self->loader()->ending(),
    #    Geoffrey::Utils::replace_spare( $self->loader()->tpl_sub(), [] )
    #);
}

sub read {
    my ($self) = @_;
    my $geoffrey = $self->_create_geoffrey;
    $geoffrey->read( $self->dir );
    $geoffrey->disconnect;
}

sub write {
    my ($self) = @_;
    my $geoffrey = $self->_create_geoffrey;
    $geoffrey->write( $self->dir );
    $geoffrey->disconnect;
}

sub _create_geoffrey {
    my ($self) = @_;
    my $dbi = 'dbi:'
      . $self->driver
      . ':database='
      . $self->db
      . ( $self->host ? ';host=' . $self->host : '' );
    require DBI;
    return Geoffrey->new(
        dbh => (
            defined $self->user
            ? DBI->connect( $dbi, $self->user, $self->pass )
            : DBI->connect($dbi)
        ),
        ( defined $self->driver ? ( changeset_driver => $self->driver ) : () ),
        ( defined $self->type   ? ( changelog_type        => $self->type )   : () ),
    );
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

App::Geoffrey::Changeset - Create a new changeset project from template for Geoffrey!

=head1 VERSION

Version 0.000100

=head1 SUBROUTINES/METHODS

=head2 read

geoffrey read [commands] [options]

=head2 write

geoffrey write [commands] [options]

=head2 _create_geoffrey

Private sub to create the Geoffrey object.

=head2 make

=head2 db

=head2 driver

=head2 changelog_type

=head2 host

=head2 pass

=head2 user

=head2 type

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, trade name, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANT ABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
