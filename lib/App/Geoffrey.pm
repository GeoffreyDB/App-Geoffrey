package App::Geoffrey;

use utf8;
use strict;
use warnings;
use Getopt::Long::Subcommand;
use Pod::Sub::Usage qw/sub2usage/;

$App::Geoffrey::VERSION = '0.000100';

my %commands = (
    converter => sub { require App::Geoffrey::Converter; App::Geoffrey::Converter->new(@_)->run; },
    changelog => sub { require App::Geoffrey::Changelog; App::Geoffrey::Changelog->new(@_)->run; },
    changeset => sub { require App::Geoffrey::Changeset; App::Geoffrey::Changeset->new(@_)->run; },
    read      => sub { require App::Geoffrey::Changeset; App::Geoffrey::Changeset->new(@_)->read; },
    write     => sub { require App::Geoffrey::Changeset; App::Geoffrey::Changeset->run(@_)->write; },
    read_help => sub { sub2usage( "read", "App::Geoffrey::Changeset" ); },
);

sub run {
    my %opts;
    my %options = (
        summary => 'Summary about your program ...',

        # common options recognized by all subcommands
        options => {
            'help|h|?' => {
                summary => 'Display help message',
                handler => sub {
                        use DDP; p @_;
                    my ( $cb, $val, $res ) = @_;
                    if ( scalar @{ $res->{_non_options_argv} } > 0 ) {
                        my $key = $res->{_non_options_argv}->[0] . '_help';
                        $commands{$key}->();
                    }
                    else {
                        require Pod::Usage;
                        Pod::Usage::pod2usage( { -verbose => 1 } );
                    }
                    exit 0;
                },
            },
            'v|version' => {
                summary => 'Display program version',
                handler => sub { print 'Geoffrey ' . $App::Geoffrey::VERSION . "\n"; exit 0; },
            },
            'd|dir=s' => \$opts{dir},
        },

        # list your subcommands here
        subcommands => {
            converter => {
                summary => 'Attach to a running container',
                options => {
                    'a|author=s'  => \$opts{author},
                    'e|email|@=s' => \$opts{email},
                    'n|name=s'    => \$opts{converter},
                },
            },
            changelog => {
                summary => 'Attach to a running container',
                options => {
                    'a|author=s'  => \$opts{author},
                    'e|email|@=s' => \$opts{email},
                    'n|name=s'    => \$opts{converter},
                },
            },
            changeset => {
                summary => 'Run a command in a new container',
                options => {
                    'a|author=s'  => \$opts{author},
                    'e|email|@=s' => \$opts{email},
                    'n|name=s'    => \$opts{converter},
                },
            },
            read => {
                summary => 'Attach to a running container',
                options => {
                    'a|author=s'    => \$opts{author},
                    'f|type=s'      => \$opts{changelog_type},
                    'r|converter=s' => \$opts{converter},
                    'b|database=s'  => \$opts{db},
                    'host=s'        => \$opts{host},
                    'user=s'        => \$opts{user},
                    'pass=s'        => \$opts{pass},
                },
            },
            write => {
                summary => 'Create a new container',
                options => {
                    'f|type=s'      => \$opts{changelog_type},
                    'r|converter=s' => \$opts{converter},
                    'b|database=s'  => \$opts{db},
                    'host=s'        => \$opts{host},
                    'user=s'        => \$opts{user},
                    'pass=s'        => \$opts{pass},
                },
            },
        },
    );

    my $res = GetOptions(%options);
    $opts{argv} = [@ARGV];
    while ( my ( $key, $val ) = each %opts ) {
        delete $opts{$key} if not defined $val;
    }
    if ( !defined $res->{subcommand} || !defined $commands{ $res->{subcommand}->[0] } ) {
        require Pod::Usage;
        Pod::Usage::pod2usage( { -verbose => 1 } );
        exit 0;
    }
    $commands{ $res->{subcommand}->[0] }->(%opts)
      if $res->{success} && $commands{ $res->{subcommand}->[0] };
}

1;

__END__

=pod

=encoding UTF-8

=head1 NAME

App::Geoffrey - Command Line module for Geoffrey

=head1 VERSION

Version 0.000100

=head1 SYNOPSIS

    geoffrey  [commands] [options]
    ...
    # start reading
    geoffrey read -db=db.sqlite -d=/path/to/changelog
    ...
    # make new converter module
    geoffrey -cd --author='Mario Zieschang' -email='mziescha [at] cpan.org' \
        -converter=MySQL -dir=/path/to/directory
    ...
    # make new file read module
    geoffrey -cf -a='Mario Zieschang' -@='mziescha [at] cpan.org' -t=XML \
        -dir=/path/to/directory
    ...
    # create a new changeset project
    geoffrey -cc -dir=/path/to/directory

=head1 OPTIONS

    Commands:
    -cd --createconverter   : Will create a module for a new converter
    -cf --createfile        : Will create a module for a new file parser
    -cc, --createchangeset  : Will create a new changeset project
    -w, --Write             : Write changeset files from existing database
    -r, --read              : Read changelog files
    -v, --version           : Print the current version of this module
    -h, --help, -?          : print what you are currently reading
    (If no command is set print what you are currently reading.)

    Options:
    -a, --author            : Author of new file or converter
    -d, --dir               : Directory of new file or converter
    -dr, --converter        : Converter to use by running changelog (default SQLite)
    -t, --type              : Changelog type for reading changesets (default Yaml)
    -db, --database         : Database to use by running changelog (default SQLite)
    -u, --user              : User to connect with remote db
    -p, --pass              : Pass for user to connect with remote db
    -e, --email, -@         : Email of author of new file or converter

=head1 SUBROUTINES/METHODS

=head2 run

=head1 SEE ALSO

=over 4

=item L<module::Starter>   The package from which the idea originated.

=back

=head1 AUTHOR

=over 4

Mario Zieschang, C<< <mziescha at cpan.org> >>

=back

=head1 LICENSE AND COPYRIGHT

=over 1

Copyright 2015 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, trade name, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANT ABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=back

=cut
